﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nottsie.Model
{
    class ListRepository : INotesRepository
    {
        List<Note> notes = new List<Note>();

        public IEnumerable<Note> GetAllNotes()
        {
            return  notes.AsReadOnly();
        }

        public Note GetNote(int position)
        {
            return this.notes[position];
        }

        public void SaveNote(Note note)
        {
            this.notes.Add(note);
        }
    }
}
