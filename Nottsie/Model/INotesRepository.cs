﻿using System.Collections.Generic;

namespace Nottsie.Model
{
    public interface INotesRepository
    {
        IEnumerable<Note> GetAllNotes();

        Note GetNote(int position);

        void SaveNote(Note note);
    }
}
