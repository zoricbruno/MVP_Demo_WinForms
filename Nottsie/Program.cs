﻿using Nottsie.Model;
using Nottsie.Presenter;
using Nottsie.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nottsie
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var repository = new ListRepository();
            var view = new View.NotesForm();
            INotesPresenter presenter = new NotesPresenter(view, repository);

            Application.Run(view);
        }
    }
}
