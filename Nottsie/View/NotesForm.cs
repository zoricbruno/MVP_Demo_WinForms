﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Nottsie.Presenter;

namespace Nottsie.View
{
    public partial class NotesForm : Form, INotesView
    {
        public NotesForm() { InitializeComponent(); }

        public NotesPresenter Presenter { private get; set; }

        public IList<string> NoteList
        {
            get { return (IList<String>)this.lbNotes.DataSource;}
            set { this.lbNotes.DataSource = value; }
        }

        public int SelectedNote
        {
            get { return this.lbNotes.SelectedIndex; }
            set { this.lbNotes.SelectedIndex = value; }
        }

        public string Title
        {
            get { return this.tbTitle.Text; }
            set { this.tbTitle.Text = value; }
        }

        public string Contents
        {
            get { return this.tbText.Text; }
            set { this.tbText.Text = value; }
        }

        public uint Importance
        {
            get { return (uint)this.nudImportance.Value; }
            set { this.nudImportance.Value = value;}
        }

        private void lbNotes_SelectedIndexChanged(object sender, EventArgs e)
        {            
            Presenter.UpdateNotesView(lbNotes.SelectedIndex);
        }

        private void bSave_Click(object sender, EventArgs e)
        {
                Presenter.SaveNote();
        }

        public void DisplayDialog(string title, string message)
        {
            MessageBox.Show(message, title);
        }
    }
}
